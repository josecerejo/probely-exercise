import { createClient } from 'pexels';

export const appConfig = {
  client: createClient('IUIctGdc2kaHioKdLXGng3WpwVMZDCBVbuHx335D9YL7WRURPoemjrh6'),
  columns: 4,
  rows: 3,
  numberOfCombinations: function () {
    return (this.columns * this.rows) / 2;
  },
};
