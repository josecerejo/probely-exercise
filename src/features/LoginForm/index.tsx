import { FC, FormEvent, useRef } from 'react';
import { useNavigate } from 'react-router-dom';
import { useAppDispatch } from '../../store/hooks';
import { updateName } from '../../store/slices/board-profile-slice';
import styles from './LoginForm.module.scss';

const LoginForm: FC = () => {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const userNameRef = useRef<HTMLInputElement>(null);

  const handleSubmit = (event: FormEvent) => {
    event.preventDefault();

    if (userNameRef.current && userNameRef.current?.value) {
      dispatch(updateName(userNameRef.current.value));
      navigate('/game');
    }
  };

  return (
    <>
      <h2 className={styles['sub-title-align']}>Type your username to start playing</h2>
      <form onSubmit={handleSubmit} className={styles.form}>
        <input ref={userNameRef} type="text" placeholder="username" />
        <button type="submit">START</button>
      </form>
    </>
  );
};

export default LoginForm;
