import { FC, useEffect } from 'react';
import { useAppDispatch, useAppSelector } from '../../store/hooks';
import { updateTimer } from '../../store/slices/board-profile-slice';
import { appConfig } from '../../utils/config';
import { formatTime } from '../../utils';

const Timer: FC = () => {
  const dispatch = useAppDispatch();
  const { active, currentTime } = useAppSelector(state => state.boardProfile);
  const { matchCards } = useAppSelector(state => state.memoryGame);

  useEffect(() => {
    let countTime: number;

    if (matchCards === appConfig.numberOfCombinations()) {
      return () => clearInterval(countTime);
    }

    if (active) {
      countTime = setInterval(() => {
        dispatch(updateTimer(currentTime + 1));
      }, 1000);
    }

    return () => clearInterval(countTime);
  }, [active, currentTime, dispatch, matchCards]);

  return (
    <>
      <p>
        <b>Timer</b>
      </p>
      <p>{formatTime(currentTime)}</p>
    </>
  );
};

export default Timer;
