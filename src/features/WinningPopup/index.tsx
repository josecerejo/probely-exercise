import { FC, useCallback } from 'react';
import styles from './WinningPopup.module.scss';
import { useAppDispatch } from '../../store/hooks';
import { restartTheGame } from '../../store/slices/ memory-game-slice';
import { restartTimer } from '../../store/slices/board-profile-slice';
import { setModalOpen } from '../../store/slices/scores-slice';

const WinningPopup: FC = () => {
  const dispatch = useAppDispatch();

  const restartGame = useCallback(() => {
    dispatch(restartTheGame());
    dispatch(restartTimer());
  }, [dispatch]);

  const checkScores = useCallback(() => {
    dispatch(setModalOpen(true));
  }, [dispatch]);

  return (
    <section className={styles.popup}>
      <h2>Congratulations we have a winner</h2>
      <div>
        <button className={styles['button-winner-popup']} onClick={checkScores}>
          Show highscores
        </button>
        |
        <button className={styles['button-winner-popup']} onClick={restartGame}>
          Start a new game
        </button>
      </div>
    </section>
  );
};

export default WinningPopup;
