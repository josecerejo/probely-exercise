import { FC, MouseEvent, useCallback, useEffect, useState } from 'react';
import styles from './MemoryGame.module.scss';
import Card from '../../components/Card';
import { useAppDispatch, useAppSelector } from '../../store/hooks';
import { setTimerActive } from '../../store/slices/board-profile-slice';
import { fetchPexelPhotos } from '../../store/actions/memory-game-action';
import {
  updatePhotos,
  updateSelectedCards,
  cleanSelectedCards,
  updateMatchCards,
} from '../../store/slices/ memory-game-slice';
import { appConfig } from '../../utils/config';
import { addScore } from '../../store/slices/scores-slice';

const MemoryGame: FC = () => {
  const dispatch = useAppDispatch();

  const timerToRevealTheCard = 1000;

  const { photos, selectedCards, matchCards } = useAppSelector(state => state.memoryGame);
  const { username, currentTime } = useAppSelector(state => state.boardProfile);

  const [isClickBlocked, setIsClickBlocked] = useState<boolean>(false);
  const [addStore, setAddStore] = useState<boolean>(false);

  const revealCard = (memoryIdGame: number) => {
    const updatedList = photos.map(photo =>
      photo.idMemoryGame === memoryIdGame ? { ...photo, statusCard: 'faceup' } : photo,
    );

    dispatch(updatePhotos(updatedList));
  };

  const hideCards = useCallback(() => {
    const [card1, card2] = photos.filter(photo => selectedCards.includes(photo.idMemoryGame));

    if (card1.id === card2.id) {
      if (appConfig.numberOfCombinations() === matchCards + 1) {
        setAddStore(true);
      }

      setIsClickBlocked(false);
      dispatch(updateMatchCards());
      return;
    }

    const faceDownCards = photos.map(photo =>
      photo.idMemoryGame === card1.idMemoryGame || photo.idMemoryGame === card2.idMemoryGame
        ? {
            ...photo,
            statusCard: 'facedown',
          }
        : photo,
    );

    setTimeout(() => {
      setIsClickBlocked(false);
      dispatch(updatePhotos(faceDownCards));
    }, timerToRevealTheCard);
  }, [dispatch, matchCards, photos, selectedCards]);

  const handleClickCard = (event: MouseEvent<HTMLImageElement>) => {
    event.stopPropagation();

    dispatch(setTimerActive(true));

    if (isClickBlocked) return;

    const cardTarget = event.target as HTMLImageElement;
    const memoryIdGame = Number(cardTarget.getAttribute('data-memory-game-id'));
    const card = photos.find(photo => photo.idMemoryGame === memoryIdGame)!;

    if (cardTarget.tagName === 'IMG' && card.statusCard === 'facedown') {
      revealCard(memoryIdGame);
      dispatch(updateSelectedCards(memoryIdGame));
    }
  };

  const checkLocalStorage = useCallback(() => {
    const memoryGameStorageData = JSON.parse(localStorage.getItem('persist:root') || '');

    if ('memoryGame' in memoryGameStorageData) {
      const photos = JSON.parse(memoryGameStorageData.memoryGame).photos;

      if (!photos.length) {
        dispatch(fetchPexelPhotos());
      }
    }
  }, [dispatch]);

  const handleSelectedCards = useCallback(() => {
    if (selectedCards.length === 2) {
      setIsClickBlocked(true);
      hideCards();
      dispatch(cleanSelectedCards());
    }
  }, [dispatch, hideCards, selectedCards.length]);

  useEffect(() => {
    checkLocalStorage();
  }, [checkLocalStorage, dispatch]);

  useEffect(() => {
    handleSelectedCards();
  }, [dispatch, handleSelectedCards, hideCards, selectedCards]);

  useEffect(() => {
    if (addStore) {
      dispatch(addScore({ username: username!, finalTime: currentTime }));
      setAddStore(false);
    }
  }, [addStore, currentTime, dispatch, username]);

  return (
    <section className={styles.container} onClick={handleClickCard}>
      {photos.map(photo => (
        <Card photo={photo} key={photo.idMemoryGame} />
      ))}
    </section>
  );
};

export default MemoryGame;
