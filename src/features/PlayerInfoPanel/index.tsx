import { FC, useCallback } from 'react';
import Timer from '../Timer';
import { useAppDispatch, useAppSelector } from '../../store/hooks';
import { setModalOpen } from '../../store/slices/scores-slice';
import { restartTimer, setTimerActive } from '../../store/slices/board-profile-slice';
import styles from './PlayerInfoPanel.module.scss';
import { useNavigate } from 'react-router-dom';
import { restartTheGame } from '../../store/slices/ memory-game-slice';

const PlayerInfoPanel: FC = () => {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const { username } = useAppSelector(state => state.boardProfile);

  const openModalScores = useCallback(() => {
    dispatch(setModalOpen(true));
    dispatch(setTimerActive(false));
  }, [dispatch]);

  const logoutUser = useCallback(() => {
    navigate('/');
    dispatch(restartTheGame());
    dispatch(restartTimer());
  }, [dispatch, navigate]);

  return (
    <aside className={styles['aside-container']}>
      <p>{username}</p>
      <Timer />
      <button onClick={openModalScores} className={styles['buttons']}>
        Show highscores
      </button>
      <button onClick={logoutUser} className={styles['buttons']}>
        Logout / Change username
      </button>
    </aside>
  );
};

export default PlayerInfoPanel;
