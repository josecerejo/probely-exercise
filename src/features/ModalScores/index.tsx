import { FC, useCallback } from 'react';
import { useAppDispatch, useAppSelector } from '../../store/hooks';
import styles from './Scores.module.scss';
import { setModalOpen } from '../../store/slices/scores-slice';
import { setTimerActive } from '../../store/slices/board-profile-slice';
import { formatTime } from '../../utils';

const ModalScores: FC = () => {
  const dispatch = useAppDispatch();

  const { scores } = useAppSelector(state => state.scores);

  const handleCloseModal = useCallback(() => {
    dispatch(setModalOpen(false));
    dispatch(setTimerActive(true));
  }, [dispatch]);

  return (
    <dialog open className={styles.dialog}>
      <div className={styles.container}>
        <div className={styles.content}>
          <div className={styles['close-modal-button']}>
            <button onClick={handleCloseModal}>Close Modal</button>
          </div>
          <h2 className={styles['dialog-title']}>HighScores</h2>
          <ol>
            {scores
              .slice()
              .sort((a, b) => a.finalTime - b.finalTime)
              .map((score, index) => (
                <li key={index} className={styles.scores}>
                  <span>{formatTime(score.finalTime)}</span>
                  <span>-</span>
                  <span>{score.username}</span>
                  <hr />
                </li>
              ))}
          </ol>
        </div>
      </div>
    </dialog>
  );
};

export default ModalScores;
