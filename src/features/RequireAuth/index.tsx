import { FC, ReactNode, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { useAppSelector } from '../../store/hooks';

interface RequireAuthProps {
  children: ReactNode;
}

const RequireAuth: FC<RequireAuthProps> = ({ children }) => {
  const navigate = useNavigate();
  const { username } = useAppSelector(state => state.boardProfile);

  useEffect(() => {
    if (!username) {
      navigate('/');
    }
  }, [navigate, username]);

  return <>{children}</>;
};

export default RequireAuth;
