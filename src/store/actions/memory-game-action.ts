import { createAsyncThunk } from '@reduxjs/toolkit';
import { Photo } from 'pexels';
import { appConfig } from '../../utils/config';

export const fetchPexelPhotos = createAsyncThunk('memoryGame/fetchPexelPhotos', async (): Promise<Photo[]> => {
  return await appConfig.client.photos
    .search({ query: 'nature', per_page: appConfig.numberOfCombinations() })
    .then(response => {
      if ('photos' in response) {
        return response.photos;
      } else {
        throw Error('There is an error with the API pexel');
      }
    });
});
