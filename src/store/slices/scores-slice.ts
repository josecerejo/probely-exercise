import { createSlice, PayloadAction } from '@reduxjs/toolkit';

type ScoreProps = {
  finalTime: number;
  username: string;
};

type StoreStateProps = {
  modalOpened: boolean;
  scores: ScoreProps[];
};

const initialState: StoreStateProps = {
  modalOpened: false,
  scores: [],
};

const scoresSlice = createSlice({
  name: 'boardProfile',
  initialState,
  reducers: {
    setModalOpen(state: StoreStateProps, { payload }: PayloadAction<boolean>) {
      state.modalOpened = payload;
    },
    addScore(state: StoreStateProps, { payload }: PayloadAction<ScoreProps>) {
      state.scores = [...state.scores, payload];
    },
  },
});

export const { addScore, setModalOpen } = scoresSlice.actions;

export default scoresSlice;
