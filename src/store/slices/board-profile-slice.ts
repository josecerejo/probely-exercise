import { createSlice, PayloadAction } from '@reduxjs/toolkit';

type BoardProfileStateProps = {
  active: boolean;
  currentTime: number;
  username?: string;
};

const initialState: BoardProfileStateProps = {
  currentTime: 0,
  active: false,
};

const boardProfileSlice = createSlice({
  name: 'boardProfile',
  initialState,
  reducers: {
    updateName(state: BoardProfileStateProps, { payload }: PayloadAction<string>) {
      state.username = payload;
    },
    setTimerActive(state: BoardProfileStateProps, { payload }: PayloadAction<boolean>) {
      state.active = payload;
    },
    updateTimer(state: BoardProfileStateProps, { payload }: PayloadAction<number>) {
      state.currentTime = payload;
    },
    restartTimer(state: BoardProfileStateProps) {
      state.currentTime = 0;
      state.active = false;
    },
  },
});

export const { setTimerActive, updateTimer, updateName, restartTimer } = boardProfileSlice.actions;

export default boardProfileSlice;
