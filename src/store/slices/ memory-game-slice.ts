import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Photo } from 'pexels';
import { fetchPexelPhotos } from '../actions/memory-game-action';

type PhotoProps = Photo & {
  statusCard: string;
  idMemoryGame: number;
};

type MemoryGameStateProps = {
  photos: PhotoProps[];
  selectedCards: number[];
  matchCards: number;
};

const initialState: MemoryGameStateProps = {
  photos: [],
  selectedCards: [],
  matchCards: 0,
};

const shufflePhotos = (photos: Photo[]) => {
  const shufflePhotos = photos
    .map((photo, index) => ({ ...photo, idMemoryGame: index + 1, statusCard: 'facedown', sort: Math.random() }))
    .sort((a, b) => a.sort - b.sort)
    .map(photo => photo);

  return shufflePhotos;
};

const memoryGameSlice = createSlice({
  name: 'boardProfile',
  initialState,
  reducers: {
    updatePhotos(state: MemoryGameStateProps, { payload }: PayloadAction<PhotoProps[]>) {
      state.photos = payload;
    },
    updateSelectedCards(state: MemoryGameStateProps, { payload }: PayloadAction<number>) {
      state.selectedCards = [...state.selectedCards, payload];
    },
    cleanSelectedCards(state: MemoryGameStateProps) {
      state.selectedCards = [];
    },
    updateMatchCards(state: MemoryGameStateProps) {
      state.matchCards = state.matchCards + 1;
    },
    restartTheGame(state: MemoryGameStateProps) {
      state.matchCards = 0;
      state.photos = shufflePhotos(state.photos);
      state.selectedCards = [];
    },
  },
  extraReducers: builder => {
    builder.addCase(fetchPexelPhotos.fulfilled, (state: MemoryGameStateProps, { payload }: PayloadAction<Photo[]>) => {
      const photos = [...payload, ...payload];

      state.photos = shufflePhotos(photos);
    });
  },
});

export const { updatePhotos, updateSelectedCards, cleanSelectedCards, updateMatchCards, restartTheGame } =
  memoryGameSlice.actions;

export default memoryGameSlice;
