import { FC, memo } from 'react';
import styles from './Card.module.scss';
import { Photo } from 'pexels';
import facedownImage from '../../assets/facedown.jpg';

interface CardProps {
  photo: Photo & { statusCard: string; idMemoryGame: number };
}

const Card: FC<CardProps> = memo(({ photo }) => {
  return (
    <img
      className={styles['image-item']}
      src={photo.statusCard === 'facedown' ? facedownImage : photo.src.medium}
      alt={'memory game image'}
      data-memory-game-id={photo.idMemoryGame}
    />
  );
});

export default Card;
