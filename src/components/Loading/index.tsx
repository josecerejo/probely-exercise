import { FC } from 'react';

const Loading: FC = () => {
  return (
    <section>
      <p>Loading</p>
    </section>
  );
};

export default Loading;
