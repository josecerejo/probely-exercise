import { FC, Suspense, lazy } from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Page404 from './pages/404';
import Login from './pages/Login';
import { Provider } from 'react-redux';
import { store, persist } from './store';
import { PersistGate } from 'redux-persist/integration/react';
import Loading from './components/Loading';

const GameLazyPage = lazy(() => import('./pages/Game'));
const ScoreLazyPage = lazy(() => import('./pages/Scores'));

const App: FC = () => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persist}>
        <BrowserRouter>
          <h1 className={'main-title'}>Awesome Memory Game</h1>
          <Suspense fallback={<Loading />}>
            <Routes>
              <Route path="/" element={<Login />} />
              <Route path="/game/scores" element={<ScoreLazyPage />} />
              <Route path="/game" element={<GameLazyPage />} />
              <Route path="*" element={<Page404 />} />
            </Routes>
          </Suspense>
        </BrowserRouter>
      </PersistGate>
    </Provider>
  );
};

export default App;
