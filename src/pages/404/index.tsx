import { FC } from 'react';
import RequireAuth from '../../features/RequireAuth';

const Page404: FC = () => {
  return <RequireAuth>Page404 not found</RequireAuth>;
};

export default Page404;
