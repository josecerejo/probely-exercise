import { FC } from 'react';
import RequireAuth from '../../features/RequireAuth';
import PlayerInfoPanel from '../../features/PlayerInfoPanel';
import MemoryGame from '../../features/MemoryGame';
import styles from './Game.module.scss';
import WinningPopup from '../../features/WinningPopup';
import { appConfig } from '../../utils/config';
import { useAppSelector } from '../../store/hooks';
import ModalScores from '../../features/ModalScores';

const Game: FC = () => {
  const { matchCards } = useAppSelector(state => state.memoryGame);
  const { modalOpened } = useAppSelector(state => state.scores);

  return (
    <RequireAuth>
      <main>
        {appConfig.numberOfCombinations() === matchCards && <WinningPopup />}
        <div className={styles.container}>
          <MemoryGame />
          <PlayerInfoPanel />
        </div>
      </main>
      {modalOpened && <ModalScores />}
    </RequireAuth>
  );
};

export default Game;
