import { FC } from 'react';
import RequireAuth from '../../features/RequireAuth';
import ModalScores from '../../features/ModalScores';

const Scores: FC = () => {
  return (
    <RequireAuth>
      <ModalScores />
    </RequireAuth>
  );
};

export default Scores;
