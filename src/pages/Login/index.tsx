import { FC } from 'react';
import LoginForm from '../../features/LoginForm';

const Login: FC = () => {
  return <LoginForm />;
};

export default Login;
